<?php
/**
 * Category page
 *
 * @package Codeus
 */

get_header();
global $more; $more = false;
?>

  </div><!-- wrap end -->

  <!-- wrap start --><div class="content-wrap">

  <?php if(have_posts()) : ?>
    <div id="main">
      <div class="central-wrapper clearfix">

        <div class="panel clearfix">

          <div id="center" class="center clearfix">
            <div id="content">
              <div class="blog_list">
                <ul class="styled">
                  <?php while(have_posts()) : the_post(); ?>
                    <?php $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'codeus_post_image'); ?>
                    <li class="clearfix">
                      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="post-info">
                          <div class="comment-info">
                            <div class="title">
                              <a href="<?php echo get_permalink($post->ID); ?>" class="date-day"><span><?php echo get_the_date('d'); ?></span></a>
                              <h3><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></h3>
                            </div>
                            <div class="date-month"><?php echo get_the_date('M'); ?></div>
                          </div>
                          <div class="inner">
                            <?php $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'codeus_post_image'); ?>
                            <?php if($image_url[0]) { ?>
                              <div class="post-image">
                                <div class="image wrap-box shadow middle">
                                  <div class="shadow-left"></div><div class="shadow-right"></div>
                                  <a href="<?php echo get_permalink($post->ID); ?>"><img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>" /></a>
                                </div>
                              </div>
                              <div class="text clearfix"><?php echo mb_substr(strip_tags(get_the_content()),0,180); ?></div>
                            <?php } else{?>
                              <div class="text clearfix"><?php echo mb_substr(strip_tags(get_the_content()),0,180); ?></div>
                            <?php }?>
                            <?php $author_id=$post->post_author;?>
                            <div class="author">
                              <a href="<?php echo get_bloginfo('url'); ?>/author/<?php echo get_the_author_meta('user_login') ?>">
                                <?php echo get_avatar(get_the_author_meta('user_email')) ?>
                                <span><?php echo get_the_author_meta('display_name') ?></span>
                              </a>
                            </div>
                            <?php
                            $tags = wp_get_post_terms($post->ID, 'geekblog_tag', array("fields" => "all"));
                            if($tags) : ?>
                              <div class="tags">
                                <?php foreach ( $tags as $tag ) {
                                  $tag_link = get_tag_link( $tag->term_id );
                                  ?>
                                  <a href="<?php echo $tag_link?>" rel="tag">#<?php echo $tag->name; ?></a>
                                <?php } ?>
                              </div>
                            <?php endif;?>
                            <?php codeus_author_info(get_the_ID()); ?>
                          </div>
                        </div>
                      </div>
                    </li>
                  <?php endwhile; ?>
                </ul><!-- /.opinion_list -->
              </div>
              <div class="paginate">
                <?php  posts_nav_link('','関連投稿' ,'次のページ'); ?>
              </div>
            </div><!-- #content -->
          </div><!-- #center -->

          <?php get_sidebar('geekblog'); ?>

        </div><!-- .panel -->

      </div><!-- .central-wrapper -->
    </div><!-- #main -->

  <?php endif; ?>

</div><!-- wrap end -->

<?php get_footer(); ?>