<?php
add_filter( 'widget_text', 'do_shortcode' );
// Create new post_type 'geekblog'

function post_type_geekblog(){
    //register taxonomy for custom post tags
    register_taxonomy(
        'geekblog_tag', //taxonomy
        'geekblog', //post-type
        array(
            'hierarchical'  => false,
            'label'         => __( 'GeekBlog Tags','taxonomy general name'),
            'singular_name' => __( 'Tag', 'taxonomy general name' ),
            'rewrite'       => true,
            'query_var'     => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true
        ));

    //register custom posts type
    $snippet_pt_args = array(
        'labels' => array(
            'name' => __( 'GeekBlog' ),
            'singular_name' => __( 'GeekBlog' )
        ),
        'public' => true,
        'hierarchical'=> false,
        'has_archive' => true,
        'show_ui' => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'taxonomies'  => array( 'geekblog-cat','geekblog_tag' ),
        'supports'    => array('title', 'editor', 'thumbnail', 'comments', 'excerpt', 'custom-fields','author','geekblog_tag'),
        'yarpp_support' => true,
    );
    register_post_type('geekblog',$snippet_pt_args);
}
add_action('init', 'post_type_geekblog');



// Create new taxonomy 'geekblog-cat'
function create_taxonomy() {
    /* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy
    */
    $labels = array(
        'name' => 'GeekBlog Category',
        'singular' => 'GeekBlog Category',
        'menu_name' => 'GeekBlog Category'
    );

    /* Biến $args khai báo các tham số trong custom taxonomy cần tạo
     */
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );

    /* Hàm register_taxonomy để khởi tạo taxonomy
     */
    register_taxonomy('geekblog-cat', 'geekblog', $args);
}

// Hook into the 'init' action
add_action( 'init', 'create_taxonomy', 0 );

function add_author_support_to_geekblog() {
    add_post_type_support( 'geekblog', 'author' );
}
add_action( 'init', 'add_author_support_to_geekblog' );

function add_custom_tag_support_to_geekblog() {
    add_post_type_support( 'geekblog', 'geekblog_tag' );
}
add_action( 'init', 'add_custom_tag_support_to_geekblog' );

// show category geekblog-cat

function geekblog_category()
{
    $args = array(
        'post_type' =>'geekblog',//optional
        'number' => '',
        'hide_empty' => 0,
        'taxonomy'  => 'geekblog-cat' //your custom taxonomy name
    );
    $categories = get_categories($args);
    ?>
    <section id="nav_menu-2" class="widget widget_nav_menu">
        <h3 class="widget-title">カテゴリー</h3>
        <div class="">
            <ul class="menu">
                <?php
                //             var_dump($categories);
                foreach($categories as $cat){
                    ?>
                    <li id="menu-item-<?php echo $cat->cat_ID?>" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-<?php echo $cat->cat_ID?>"><a href="<?php echo get_term_link($cat->cat_ID,'geekblog-cat'); ?>"><?php echo $cat->name?></a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </section>
    <?php

}

add_shortcode("geekblog_category", "geekblog_category");


function geekblog_tags() {
    $args = array(
        'post_type' =>'geekblog',//optional
        'number' => '',
        'hide_empty' => 0,
        'order' => 'DESC',
        'post_status' => 'publish',
        'taxonomy'  => 'geekblog_tag' //your custom taxonomy name
    );
    $tags = get_categories($args);
    ?>
    <section id="nav_menu-3" class="widget widget_nav_menu">
        <h3 class="widget-title">タグ</h3>
        <div class="">
            <ul class="menu">
                <?php
                //             var_dump($categories);
                foreach($tags as $tag){
                    ?>
                    <li id="menu-item-<?php echo $tag->cat_ID?>" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-<?php echo $tag->cat_ID?>"><a href="<?php echo get_term_link($tag->cat_ID,'geekblog_tag'); ?>"><?php echo $tag->name?></a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </section>
    <?php

}
add_shortcode("geekblog_tags", "geekblog_tags");

function post_tags() {
    $args = array(
        'post_type' =>'post',//optional
        'number' => '',
        'hide_empty' => 0,
        'order' => 'DESC',
        'post_status' => 'publish',
        'taxonomy'  => 'post_tag' //your custom taxonomy name
    );
    $tags = get_categories($args);
    ?>
    <section id="nav_menu-3" class="widget widget_nav_menu">
        <h3 class="widget-title">タグ</h3>
        <div class="">
            <ul class="menu">
                <?php
                //             var_dump($categories);
                foreach($tags as $tag){
                    ?>
                    <li id="menu-item-<?php echo $tag->cat_ID?>" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-<?php echo $tag->cat_ID?>"><a href="<?php echo get_term_link($tag->cat_ID,'post_tag'); ?>"><?php echo $tag->name?></a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </section>
    <?php

}
add_shortcode("post_tags", "post_tags");
function geekblog_archive() {
    $args = array(
        'type'            => 'monthly',
        'limit'           => '',
        'format'          => 'html',
        'before'          => '',
        'after'           => '',
        'show_post_count' => true,
        'echo'            => 1,
        'order'           => 'DESC',
        'post_type'     => 'geekblog'
    );
    ?>
    <section id="archives-3" class="widget widget_archive">
        <h3 class="widget-title">月別で見る</h3>
        <ul>
            <?php
            wp_get_archives( $args );
            ?>
        </ul>
    </section>
    <?php

}
add_shortcode("geekblog_archive", "geekblog_archive");

function geekblog_author() {
    global $wpdb;
    $authors=$wpdb->get_results('select `'.$wpdb->prefix.'users`.`ID`, `'.$wpdb->prefix.'users`.`user_login`, `'.$wpdb->prefix.'users`.`display_name` from `'.$wpdb->prefix.'users` inner join `'.$wpdb->prefix.'posts` on `'.$wpdb->prefix.'users`.`ID`=`'.$wpdb->prefix.'posts`.`post_author` where `post_type`=\'geekblog\' and `post_status`=\'publish\'  group by `'.$wpdb->prefix.'users`.`ID`;');
    ?>
    <section id="authors" class="widget widget_author">
        <h3 class="widget-title">投稿者から見る</h3>
        <ul class="menu">
            <?php
                foreach($authors as $user){
                    ?>
                     <li>
                        <div class="author">
                            <a href="<?php echo get_bloginfo('url'); ?>/author/<?php echo $user->user_login ?>">
                                <?php echo get_avatar($user->ID); ?>
                                <span><?php echo $user->display_name ?></span>
                            </a>
                        </div>
                    </li>
                    <?php
                }
            ?>
        </ul>
    </section>
    <?php
}
add_shortcode("geekblog_author", "geekblog_author");

